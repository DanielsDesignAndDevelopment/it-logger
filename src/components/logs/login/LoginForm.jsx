import React, {useState} from 'react';
import env from 'react-dotenv';
import { connect } from 'react-redux';
import { Form, Alert } from 'react-bootstrap';
import { logIn } from '../../../actions/logActions';
import PropTypes from 'prop-types';

function LoginForm ({ logIn, loggedIn }) {

  const [userEmail, setUserEmail] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [alert, setAlert] = useState(false);

  const loginEmail = env.USER_NAME;
  const loginPassword = env.PASSWORD;

  const handleEmailChange = (e) => {
    setUserEmail(e.target.value);
  }

  const handlePasswordChange = (e) => {
    setUserPassword(e.target.value);
  }

  const handleSubmit = () => {
    if (!(userEmail === loginEmail && userPassword === loginPassword)) {
      setAlert(true);
      setTimeout(() => setAlert(false), 3000);
    } else {
      logIn();
    }
  }

  return (
    <Form style={formStyle}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email Address</Form.Label>
        <Form.Control 
          type="email" 
          placeholder="Enter email"
          value={userEmail}
          onChange={handleEmailChange}
        />          
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control 
          type="password" 
          placeholder="Enter Password"
          value={userPassword}
          onChange={handlePasswordChange}
        />
      </Form.Group>
      {alert && <Alert variant="danger" className="text-center">Invalid Password or Username</Alert>}
      {loggedIn 
        ? <Alert variant="primary" className="text-center">You are logged in</Alert>
        : <button
            className="btn btn-block bg-primary w-100 border-0"
            type="button"
            onClick={handleSubmit}
          >
            Login to edit app
          </button>}
    </Form>
  )
}

const formStyle = {
  border: "none",
  width: "100%"
}

LoginForm.propTypes = {
  logIn: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({
  loggedIn: state.log.loggedIn
})

export default connect(mapStateToProps, {logIn})(LoginForm);