import React, { useState, Fragment} from 'react';
import { Row, Col } from 'react-bootstrap';
import NavLinks from './Navlinks';
import LoginButton from './LoginButton';
import LoginModal from './LoginModal';

function Login () {

  const [showLoginModal, setShowLoginModal] = useState(false);

  return (
    <Fragment>
      <Row>
        <Col className="col-12 col-md-2">
          <LoginButton onClick={() => setShowLoginModal(true)} />
        </Col>
        <Col className="col-12 col-md-8">
          <NavLinks />
        </Col>
      </Row>
      <LoginModal show={showLoginModal} onHide={() => setShowLoginModal(false)}/>
    </Fragment>
  )
}

export default Login;