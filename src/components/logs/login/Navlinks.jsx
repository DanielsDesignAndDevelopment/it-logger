import React from 'react';
import { FaBitbucket } from 'react-icons/fa';

function NavLinks () {
  return (
    <div className="text-center">
      <a 
        href="https://bitbucket.org/DanielsDesignAndDevelopment/it-logger"
        style={linkStyle}       
      >
        <FaBitbucket style={iconStyle} /> See code
      </a>
      <span style={iconStyle} className="text-primary">|</span>
      <a href="https://danielsdnd.com" style={linkStyle}>
        Back to danielsdnd.com
      </a>
    </div>
  )
}

const linkStyle = {
  textDecoration: "none",
  margin: "10px"
}

const iconStyle = {
  position: "relative",
  top: "-2px"
}

export default NavLinks;