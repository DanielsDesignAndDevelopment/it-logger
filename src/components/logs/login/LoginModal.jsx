import React from 'react';
import { Modal } from 'react-bootstrap';
import LoginForm from './LoginForm';

function LoginModal (props) {
   return (
    <Modal
      {...props}
      size="xl"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      style={modalStyle}
      animation={false}
    >
      <Modal.Header style={{paddingLeft: "0px"}}>
        <Modal.Title id="contained-modal-title-vcenter">
          Login to Add/Edit Work Orders
        </Modal.Title>
      </Modal.Header>
      <LoginForm />
      <Modal.Footer className="w-100 px-0 border-0">
        <button 
          onClick={props.onHide}
          className="btn btn-block bg-primary w-100"
          type="button"
        >
          Close
        </button>
      </Modal.Footer>
    </Modal>
  )
}

const modalStyle = {
  marginTop: "100px"
}

export default LoginModal;