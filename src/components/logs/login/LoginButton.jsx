import React from 'react';
import PropTypes from 'prop-types';

function LoginButton ({onClick}) {
  return (
    <button 
      onClick={onClick}
      className="bg-primary text-white mx-3"
      style={BtnStyle}
    >
      Login
    </button>
  )
}

const BtnStyle = {
  height: "auto",
  width: "100px",
  padding: "10px",
  borderRadius: "5px",
  border: "none",
  marginBottom: "25px"
}

LoginButton.propTypes = {
  onClick: PropTypes.func
}

export default LoginButton;