import React from 'react';
import Moment from 'react-moment';
import { Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { deleteLog, setCurrent } from '../../actions/logActions';
import M from 'materialize-css/dist/js/materialize.min.js';

function LogItem({ log, loggedIn, deleteLog, setCurrent }) {
  const { attention, date, id, message, tech } = log;

  const onDelete = () => {
    if (loggedIn){
      deleteLog(id);
      M.toast({ html: 'Log Deleted' })
    } else {
      M.toast({html: 'Log in to delete this log'})
    }
  }

  return (
    <li className='collection-item'>
      <div>
        <Row className="mb-0">
          <Col>
            <a
              href='#edit-log-modal'
              className={`modal-trigger ${attention ? 'red-text' : 'blue-text'}`}
              onClick={() => setCurrent(log)}
            >
              {message}
            </a>
            <br />
          </Col>
        </Row>
        <Row className="my-0">
          <Col className="col-10">
            <span className='grey-text'>
              <span className="black-text">ID #{id}</span> last updated by {' '}
              <span className="black-text">{tech}</span> on <Moment format='MMMM Do YYYY, h:mm:ss a'>{date}</Moment>
            </span>
          </Col>
          <Col>
            <a href="#!" onClick={onDelete} className="secondary-content">
              <i className="material-icons grey-text">delete</i>
            </a>
          </Col>
        </Row>
      </div>
    </li>
  )
}

LogItem.propTypes = {
  log: PropTypes.object.isRequired,
  loggedIn: PropTypes.bool.isRequired,
  deleteLog: PropTypes.func.isRequired,
  setCurrent: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  loggedIn: state.log.loggedIn
})

export default connect(mapStateToProps, { deleteLog, setCurrent })(LogItem);