import React from 'react';
import { Col, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';
import TechSelectOptions from '../../techs/TechSelectOptions';

function EditAssignedTech ({value, onChange}) {
  return (
    <Row>
      <Col>
        <div className='input-field'>
          <select
            name='tech'
            value={value}
            className='browser-default'
            onChange={onChange}
          >
            <option value='' disabled>
              Select Technician
            </option>
            <TechSelectOptions />
          </select>
        </div>
      </Col>
    </Row>
  )
}

EditAssignedTech.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func
}

export default EditAssignedTech;