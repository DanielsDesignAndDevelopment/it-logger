import React from 'react';
import { Col, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';

function EditNeedsAttention ({checked, value, onChange}) {
  return (
    <Row>
      <Col>
        <div className="input-field">
          <p>
            <label>
              <input
                type='checkbox'
                className='filled-in'
                checked={checked}
                value={value}
                onChange={onChange}
              />
              <span>Needs Attention</span>
            </label>
          </p>
        </div>
      </Col>
    </Row>
  )
}

EditNeedsAttention.propTypes = {
  checked: PropTypes.bool,
  value: PropTypes.bool,
  onChange: PropTypes.func
}

export default EditNeedsAttention;