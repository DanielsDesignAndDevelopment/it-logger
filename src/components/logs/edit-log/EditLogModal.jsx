import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Form, Alert } from 'react-bootstrap';
import PropTypes from 'prop-types';
import M from 'materialize-css/dist/js/materialize.min.js';
import { updateLog } from '../../../actions/logActions';
import EditWork from './EditWork';
import EditAssignedTech from './EditAssignedTech';

import EditNeedsAttention from './EditNeedsAttention';

function EditLogModal({ current, updateLog, loggedIn }) {
  const [message, setMessage] = useState('');
  const [attention, setAttention] = useState(false);
  const [tech, setTech] = useState('');
  const [loginAlert, setLoginAlert] = useState(false);
  const [missingInfoAlert, setMissingInfoAlert] = useState(false);

  useEffect(() => {
    if (current) {
      setMessage(current.message);
      setAttention(current.attention);
      setTech(current.tech);
    }
  }, [current])

  const onSubmit = (e) => {
    e.preventDefault();

    if(!loggedIn) {
      setLoginAlert(true);
      setTimeout(() => setLoginAlert(false), 3000)
    } else if (message === '' || tech === '') {
      setMissingInfoAlert(true);
      setTimeout(() => setMissingInfoAlert(false), 3000)
    } else {
      const updatedLog = {
        id: current.id,
        message,
        attention,
        tech,
        date: new Date(),
      }

      updateLog(updatedLog);
      M.toast({ html: `Log updated by ${tech}` });

      //Clear fields
      setMessage('');
      setTech('');
      setAttention(false);
    }
  }

  return (
    <div id='edit-log-modal' className='modal' style={modalStyle}>
      <div className='modal-content border-bottom-0'>
        <Form>
          <Row>
            <Col>
              <h4>Edit System Log</h4>
            </Col>
          </Row>
          <EditWork value={message} onChange={e => setMessage(e.target.value)} />
          <EditAssignedTech value={tech} onChange={e => setTech(e.target.value)} />
          <EditNeedsAttention 
            checked={attention} 
            value={attention} 
            onChange={e => setAttention(!attention)}
          />
        </Form>
        {loginAlert && <Alert className="text-center" variant="danger">Login to edit work</Alert>}
        {missingInfoAlert && <Alert className="text-center" variant="danger">Please enter work and a technician</Alert>}
        <a 
            href="#!" 
            onClick={onSubmit} 
            className="bg-primary btn"
        >
            Enter
        </a>
        <button className="text-center modal-close btn btn-block my-2 bg-primary">
          Close
        </button>
      </div>
    </div>
  )
}

const modalStyle = {
  width: '75%',
  height: '75%'
}

EditLogModal.propTypes = {
  current: PropTypes.object,
  updateLog: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({
  current: state.log.current,
  loggedIn: state.log.loggedIn
})

export default connect(mapStateToProps, { updateLog })(EditLogModal);