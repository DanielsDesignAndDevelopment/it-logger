import React from 'react';
import { Col, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';

function EditWork ({value, onChange}) {
  return (
    <Row>
      <Col>
        <div className='input-field'>
          <input
            type='text'
            name='message'
            value={value}
            onChange={onChange}
          />
        </div>
      </Col>
    </Row>
  )
}

EditWork.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func
}

export default EditWork;