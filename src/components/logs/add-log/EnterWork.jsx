import React from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import PropTypes from 'prop-types';

function EnterWork ({onChange, value}) {
  return (
    <Row>
      <Col>
        <Form.Control 
        type="text" 
        placeholder="Enter workorder or log completed work"
        name="message"
        className="w-100"
        value={value}
        onChange={onChange}
        />  
      </Col>     
    </Row>
  )
}

EnterWork.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string,
}

export default EnterWork;