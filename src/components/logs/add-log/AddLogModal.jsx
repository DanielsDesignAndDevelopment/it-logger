import React, { useState } from 'react';
import { Form, Row, Col, Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addLog } from '../../../actions/logActions';
import M from 'materialize-css/dist/js/materialize.min.js';
import EnterWork from './EnterWork';
import ChooseTech from './ChooseTech';
import NeedsAttention from './NeedsAttention';

function AddLogModal({ addLog, loggedIn }) {
  const [message, setMessage] = useState('');
  const [attention, setAttention] = useState(false);
  const [tech, setTech] = useState('');
  const [loginAlert, setLoginAlert] = useState(false);
  const [missingInfoAlert, setMissingInfoAlert] = useState(false);

  const onSubmit = (e) => {
    e.preventDefault();

    if (!loggedIn) {
      setLoginAlert(true);
      setTimeout(() => setLoginAlert(false), 3000);
    } else if (message === '' || tech === '') {
      setMissingInfoAlert(true);
      setTimeout(() => setMissingInfoAlert(false), 3000);
    } else {
      const newLog = {
        message,
        attention,
        tech,
        date: new Date()
      }

      addLog(newLog);

      M.toast({ html: `Log added by ${tech}` });

      // Clear Fields
      setMessage('');
      setTech('');
      setAttention(false);
    }
  }

  return (
    <div id='add-log-modal' className='modal' style={modalStyle}>
      <div className="modal-content border-bottom-0">        
        <Form>
          <Row>
            <Col>
              <h4>Enter System Log</h4>
            </Col>
          </Row>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <EnterWork value={message} onChange={e => setMessage(e.target.value)} />
            <ChooseTech value={tech} onChange={e => setTech(e.target.value)} />
            <NeedsAttention 
              value={attention} 
              checked={attention} 
              onChange={e => setAttention(!attention)} />
          </Form.Group>
        </Form>
        {loginAlert 
          && <Alert variant="danger" className="text-center">Log in to add work</Alert>}
        {missingInfoAlert 
          && <Alert variant="danger" className="text-center">Please enter work and/or select a technician</Alert>}
        <a href="#!" 
          onClick={onSubmit} 
          className="waves-effect bg-primary btn">Enter</a>
          <button 
            className="btn bg-primary waves-effect btn-block my-2 modal-close"
          >
            Close
          </button>
      </div>
    </div>
  )
}

AddLogModal.propTypes = {
  addLog: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool.isRequired
}

const modalStyle = {
  width: '75%',
  height: '75%'
}

const mapStateToProps = state => ({loggedIn: state.log.loggedIn});

export default connect(mapStateToProps, { addLog })(AddLogModal);