import React from 'react';
import { Row, Col } from 'react-bootstrap';
import TechSelectOptions from '../../techs/TechSelectOptions';
import PropTypes from 'prop-types';

function ChooseTech ({value, onChange}) {
  return (
    <Row>
      <Col className="col-sm-8 col-md-6 col-lg-4">
        <div className="input-field">
          <select
            name="tech"
            value={value}
            className="browser-default"
            onChange={onChange}>
            <option value="" disabled>Select Technician</option>
            <TechSelectOptions />
          </select>
        </div>
      </Col>
    </Row>
  )
}

ChooseTech.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func
}

export default ChooseTech;