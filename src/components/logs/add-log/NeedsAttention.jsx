import React from 'react';
import { Col, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';

function NeedsAttention ({checked, value, onChange}) {
  return (
    <Row>
      <Col>
        <div className="input-field">
          <div>
            <label>
              <input
                type='checkbox'
                className='filled-in'
                checked={checked}
                value={value}
                onChange={onChange}
              />
              <span>Needs Attention</span>
            </label>
          </div>
        </div>
      </Col>
    </Row>
  )
}

NeedsAttention.propTypes = {
  checked: PropTypes.bool,
  onChange: PropTypes.func
}

export default NeedsAttention;