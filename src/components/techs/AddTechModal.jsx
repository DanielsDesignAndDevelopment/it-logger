import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Form, Alert } from 'react-bootstrap';
import PropTypes from 'prop-types'
import { addTech } from '../../actions/techActions';
import M from 'materialize-css/dist/js/materialize.min.js';

function AddTechModal({ addTech, loggedIn }) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [loginAlert, setLoginAlert] = useState(false);
  const [missingInfoAlert, setMissingInfoAlert] = useState(false);

  const onSubmit = () => {
    if (!loggedIn) {
      setLoginAlert(true);
      setTimeout(() => setLoginAlert(false), 3000);
    } else if (firstName === '' || lastName === '') {
      setMissingInfoAlert(true);
      setTimeout(() => setMissingInfoAlert(false), 3000);
    } else {
      addTech({
        firstName,
        lastName,
      })

      M.toast({ html: `${firstName} ${lastName} was added as a tech.` })

      // Clear Fields
      setFirstName('');
      setLastName('');
    }
  }

  return (
    <div id='add-tech-modal' className='modal'>
      <div className="modal-content border-bottom-0">
        <Form>
          <Row>
            <Col>
              <h4>New Technician</h4>
            </Col>
          </Row>
          <Row>
            <Col className="w-80">
              <div className="input-field">
                <input
                  type='text'
                  name='firstName'
                  value={firstName}
                  onChange={e => setFirstName(e.target.value)}
                />
                <label htmlFor='firstName' className='active'>
                  First Name
                </label>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
            <div className="input-field">
              <input
                  type='text'
                  name='lastName'
                  value={lastName}
                  onChange={e => setLastName(e.target.value)}
              />
              <label htmlFor='lastName' className='active'>
                  Last Name
              </label>
            </div>
            </Col>
          </Row>
        </Form>
        {loginAlert && <Alert className="text-center" variant="danger">Log in to add technician</Alert>}
        {missingInfoAlert && <Alert className="text-center" variant="danger">Please enter a technician</Alert>}
        <a 
          href="#!" 
          onClick={onSubmit} 
          className=" waves-effect btn bg-primary btn-block"
        >
          Enter
        </a>
        <button className="modal-close btn waves-effect bg-primary btn-block my-2">Close</button>
      </div>
    </div>
  )
}

AddTechModal.propTypes = {
  addTech: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({loggedIn: state.log.loggedIn})

export default connect(mapStateToProps, { addTech })(AddTechModal);