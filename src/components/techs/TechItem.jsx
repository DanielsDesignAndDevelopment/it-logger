import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { deleteTech } from '../../actions/techActions';
import M from 'materialize-css/dist/js/materialize.min.js';

function TechItem({ tech: { firstName, lastName, id }, deleteTech, loggedIn }) {
  const onDelete = () => {
    if (loggedIn) {
      deleteTech(id);
      M.toast({ html: 'Technician deleted' })
    } else {
      M.toast({html: "Log in to delete technician"})
    }    
  }
  return (
    <li className='collection-item'>
      <div>
        {firstName} {lastName}
        <a href='#!' className='secondary-content' onClick={onDelete}>
          <i className="material-icons grey-text">delete</i>
        </a>
      </div>
    </li>
  )
}

TechItem.propTypes = {
  tech: PropTypes.object.isRequired,
  deleteTech: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({loggedIn: state.log.loggedIn})

export default connect(mapStateToProps, { deleteTech })(TechItem);