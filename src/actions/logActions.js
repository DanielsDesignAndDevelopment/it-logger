import {
    GET_LOGS,
    SET_LOADING,
    LOGS_ERROR,
    ADD_LOG,
    DELETE_LOG,
    SET_CURRENT,
    CLEAR_CURRENT,
    UPDATE_LOG,
    SEARCH_LOGS,
    LOG_IN
} from './types';

// Get logs from server
export const getLogs = () => async dispatch => {
    try {
        setLoading();
        const res = await fetch('https://d3-it-logger-json-server.herokuapp.com/logs');
        const data = await res.json(); // extracts JSON body content from response

        dispatch({
            type: GET_LOGS,
            payload: data,
        })
    } catch (err) {
        dispatch({
            type: LOGS_ERROR,
            payload: err.response.data,
        })
    }

}

// add new log
export const addLog = (log) => async dispatch => {
    try {
        setLoading();

        const res = await fetch('https://d3-it-logger-json-server.herokuapp.com/logs', {
            method: 'POST',
            body: JSON.stringify(log),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        const data = await res.json();

        dispatch({
            type: ADD_LOG,
            payload: data,
        })

    } catch (err) {
        dispatch({
            type: LOGS_ERROR,
            payload: err.response.statusText,
        })
    }
}

// Delete log from server 
export const deleteLog = (id) => async dispatch => {
    try {
        setLoading();

        await fetch(`https://d3-it-logger-json-server.herokuapp.com/logs/${id}`, {
            method: 'DELETE'
        });

        dispatch({
            type: DELETE_LOG,
            payload: id,
        })

    } catch (err) {
        dispatch({
            type: LOGS_ERROR,
            payload: err.response.statusText,
        })
    }
}

// Update log on server
export const updateLog = log => async dispatch => {
    const { id } = log;
    try {
        setLoading();
        const res = await fetch(`https://d3-it-logger-json-server.herokuapp.com/logs/${id}`, {
            method: 'PUT',
            body: JSON.stringify(log),
            headers: {
                'Content-Type': 'application/json'
            }
        })

        const data = await res.json();

        dispatch({
            type: UPDATE_LOG,
            payload: data,
        })

    } catch (err) {
        dispatch({
            type: LOGS_ERROR,
            payload: err.response.data,
        })
    }
}

export const searchLogs = (text) => async dispatch => {
    try {
        setLoading();

        const res = await fetch(`https://d3-it-logger-json-server.herokuapp.com/logs?q=${text}`);
        const data = await res.json();

        dispatch({
            type: SEARCH_LOGS,
            payload: data
        })
    } catch (err) {
        dispatch({
            type: LOGS_ERROR,
            payload: err.response.statusText
        })
    }
}

// Set current log
export const setCurrent = (log) => {
    return {
        type: SET_CURRENT,
        payload: log
    }
}

// Clear current log
export const clearCurrent = () => {
    return {
        type: CLEAR_CURRENT,
    }
}

// Set loading to true
export const setLoading = () => {
    return {
        type: SET_LOADING
    }
}

export const logIn = () => {
    return {
        type: LOG_IN
    }
}
